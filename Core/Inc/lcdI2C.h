/*
 * lcdI2C.h
 *
 *  Created on: Jun 19, 2023
 *      Author: mariano
 */

#ifndef INC_LCDI2C_H_
#define INC_LCDI2C_H_

#include <stdint.h>

#define CLEAR_DISPLAY 0x01
#define DDRAM 0x80

void lcd_loop(uint8_t * data_t);
void lcd_send_cmd(char cmd);
void lcd_send_data(char data);
void lcd_init (void);
void lcd_clear(void);
void lcd_send_string(char *str);
void lcd_tick(void);
void lcd_goto_xy(int row, int col);  // put cursor at the entered position row (0 or 1), col (0-15);



#endif /* INC_LCDI2C_H_ */
