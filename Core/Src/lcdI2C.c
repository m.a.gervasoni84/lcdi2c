#include "lcdI2C.h"
#include "main.h"
#include <stdbool.h>
#include <strings.h>

static char data_u, data_l;
static uint8_t data_t[4];

uint32_t state = 0;

bool tx_done_LCD = false;
static bool tx_done;

static HAL_StatusTypeDef rv;

uint32_t lcd_counter_ms, lcd_counter_ms;

char str[40];

#define SLAVE_ADDRESS_LCD 0x4E //(01001110) direccion por default del PCF8574 con los pines A0,A1,A2 de la placa se cambia
/*Tambien puede ser 0x7E, entonces sera el caso de un PCF8574A*/

//void lcd_loop(uint8_t *data_t) {
//	HAL_StatusTypeDef rv;
//	state = 0;
//
//		switch (state) {
//		case 0:
//			rv = HAL_I2C_Master_Transmit_IT(&hi2c1, SLAVE_ADDRESS_LCD,
//					data_t, 4);
//			if (HAL_OK == rv) {
//				lcd_counter_ms = 5;
//				state = 1;
//			}
//			break;
//		case 1:
//			if (lcd_counter_ms == 0 && tx_done) {
//				tx_done = false;
//				state = 2;
//			} else
//				break;
//		case 2:
//			//HAL_Delay(15);
//			break;
//
//	}
//}
void HAL_I2C_MasterTxCpltCallback(I2C_HandleTypeDef *hi2c1) {
	tx_done = 1;
}

void lcd_send_cmd(char cmd) {
	data_u = (cmd & 0xf0);
	data_l = (cmd << 4);
	data_t[0] = data_u | 0x0C; //en=1, rs=0
	data_t[1] = data_u | 0x08; //en=0, rs=0
	data_t[2] = data_l | 0x0C; //en=1, rs=0
	data_t[3] = data_l | 0x08; //en=0, rs=0
	rv = HAL_I2C_Master_Transmit_IT(&hi2c1, SLAVE_ADDRESS_LCD, data_t, 4);
	if (HAL_OK == rv) {
		lcd_counter_ms = 5;
		if (lcd_counter_ms == 0 && tx_done) {
			tx_done = 0;
			bzero(data_t, 4);
		}
	}
}

void lcd_send_data(char data) {
	data_u = (data & 0xf0);
	data_l = ((data << 4) & 0xf0);
	data_t[0] = data_u | 0x0D; //en=1, rs=1
	data_t[1] = data_u | 0x09; //en=0, rs=1
	data_t[2] = data_l | 0x0D; //en=1, rs=1
	data_t[3] = data_l | 0x09; //en=0, rs=1
	rv = HAL_I2C_Master_Transmit_IT(&hi2c1, SLAVE_ADDRESS_LCD, data_t, 4);
	if (HAL_OK == rv) {
		lcd_counter_ms = 5;
		if (lcd_counter_ms == 0 && tx_done) {
			tx_done = 0;

		}
	}
}

void lcd_init(void) {
	// 4 bit inicializacion
	lcd_counter_ms = 1;  // wait for >40ms
	lcd_send_cmd(0x30);
	lcd_counter_ms = 5;  // wait for >4.1ms
	lcd_send_cmd(0x30);
	lcd_counter_ms = 1;  // wait for >100us
	lcd_send_cmd(0x30);
	lcd_counter_ms = 1;
	lcd_send_cmd(0x20);  // 4bit mode
	lcd_counter_ms = 1;

	// dislay initialisation
	lcd_send_cmd(0x28); // Function set --> DL=0 (4 bit mode), N = 1 (2 line display) F = 0 (5x8 characters)
	lcd_counter_ms = 1;
	lcd_send_cmd(0x08); //Display on/off control --> D=0,C=0, B=0  ---> display off
	lcd_counter_ms = 1;
	lcd_send_cmd(0x01);  // clear display
	lcd_counter_ms = 1;
	lcd_counter_ms = 1;
	lcd_send_cmd(0x06); //Entry mode set --> I/D = 1 (increment cursor) & S = 0 (no shift)
	lcd_counter_ms = 1;
	lcd_send_cmd(0x0C); //Display on/off control --> D = 1, C and B = 0. (Cursor and blink, last two bits)
}

void lcd_send_string(char *str) {
	while (*str) {
		lcd_send_data(*str++);
	}
}

void lcd_tick(void) {
	if (lcd_counter_ms > 0) {
		lcd_counter_ms--;
	}
}

//void lcd_send_cmd (char cmd)
//{
//	HAL_StatusTypeDef rv;
//	data_u = (cmd&0xf0);
//	data_l = ((cmd<<4)&0xf0);
//	data_t[0] = data_u|0x0C;  //en=1, rs=0
//	data_t[1] = data_u|0x08;  //en=0, rs=0
//	data_t[2] = data_l|0x0C;  //en=1, rs=0
//	data_t[3] = data_l|0x08;  //en=0, rs=0
//	rv = HAL_I2C_Master_Transmit_IT (&hi2c1, SLAVE_ADDRESS_LCD,(uint8_t *) data_t, 4);
//	if ( HAL_OK == rv )
//	{
//		counterms = 1;
//		if ( counterms == 0 && tx_done_LCD)
//		tx_done_LCD = false;
//
//	}
//}
//
//void lcd_send_data (char data)
//{
//	HAL_StatusTypeDef rv;
//	data_u = (data&0xf0);
//	data_l = ((data<<4)&0xf0);
//	data_t[0] = data_u|0x0D;  //en=1, rs=0
//	data_t[1] = data_u|0x09;  //en=0, rs=0
//	data_t[2] = data_l|0x0D;  //en=1, rs=0
//	data_t[3] = data_l|0x09;  //en=0, rs=0
//	rv = HAL_I2C_Master_Transmit_IT (&hi2c1, SLAVE_ADDRESS_LCD,(uint8_t *) data_t, 4);
//
//	if ( HAL_OK == rv )
//	{
//		counterms = 1;
//		if ( counterms == 0 && tx_done_LCD)
//				tx_done_LCD = false;
//	}
//}
//
//void lcd_clear (void)
//{
//	lcd_send_cmd (CLEAR_DISPLAY);
//}
//
//void lcd_put_cur (unsigned char x, unsigned char y)
//{
//	char position;
//    if ( y == '0')
//    {
//            position = DDRAM;
//    }
//    else
//    {
//            position = DDRAM|0x40;
//    }
//
//    position += (x+1);
//    lcd_send_cmd ( position );
//}
//
//
//void lcd_init (void)
//{
//	// 4 bit initialisation
//	counterms = 50;  // wait for >40ms
//	lcd_send_cmd (0x30);
//	counterms = 5;  // wait for >4.1ms
//	lcd_send_cmd (0x30);
//	counterms = 1;  // wait for >100us
//	lcd_send_cmd (0x30);
//	lcd_send_cmd (0x20);  // 4bit mode
//
//  // dislay initialisation
//	lcd_send_cmd (0x28); // Function set --> DL=0 (4 bit mode), N = 1 (2 line display) F = 0 (5x8 characters)
//	lcd_send_cmd (0x08); //Display on/off control --> D=0,C=0, B=0  ---> display off
//	lcd_send_cmd (CLEAR_DISPLAY);  // clear display
//	lcd_send_cmd (0x06); //Entry mode set --> I/D = 1 (increment cursor) & S = 0 (no shift)
//	lcd_send_cmd (0x0C); //Display on/off control --> D = 1, C and B = 0. (Cursor and blink, last two bits)
//}
//
//void lcd_send_string (char *str)
//{
//	for(i=0;str[i]!='\0';i++)
//	{
//		lcd_send_data(str[i]);
//	}
//}
//

//void lcd_tick (void)
//{
//	if (counterms > 0)
//	{
//		counterms--;
//	}
//}
